package top.hmtools.gbt2260.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import top.hmtools.GBT2260;
import top.hmtools.beans.DivisionBean;

@Controller
@RequestMapping(value="/region")
public class RegionController {

	/**
	 * 获取所有省份及其所辖市、区、县信息
	 * @return
	 */
	@RequestMapping(value="/province/full",method=RequestMethod.GET)
	public ResponseEntity<List<DivisionBean>> provinceFull(){
		List<DivisionBean> divisionBeans = GBT2260.getAllProvince();
		ResponseEntity<List<DivisionBean>> result = new ResponseEntity<List<DivisionBean>>(divisionBeans, HttpStatus.OK	);
		return result;
	}
	
	/**
	 * 获取所有仅省份信息（无其所辖市、区、县信息）
	 * @return
	 */
	@RequestMapping(value="/province/simple",method=RequestMethod.GET)
	public ResponseEntity<List<DivisionBean>> provinceSimple(){
		List<DivisionBean> divisionBeans = GBT2260.getAllOnlyProvince();
		ResponseEntity<List<DivisionBean>> result = new ResponseEntity<List<DivisionBean>>(divisionBeans, HttpStatus.OK	);
		return result;
	}

	/**
	 * 根据省名称或者行政区域编号获取其所辖的所有市信息集合（不含其所辖的区、县信息）
	 * @param keyword
	 * @return
	 */
	@RequestMapping(value="/city/simple",method=RequestMethod.GET)
	public ResponseEntity<List<DivisionBean>> citySimple(@RequestParam(name="keyword")String keyword){
		List<DivisionBean> divisionBeans = GBT2260.getAllOnlyCityByProvince(keyword);
		ResponseEntity<List<DivisionBean>> result = new ResponseEntity<List<DivisionBean>>(divisionBeans, HttpStatus.OK	);
		return result;
	}

	/**
	 * 根据市名称或者行政区域编号获取其所辖的所有区、县信息集合
	 * @param keyword
	 * @return
	 */
	@RequestMapping(value="/district/simple",method=RequestMethod.GET)
	public ResponseEntity<List<DivisionBean>> districtSimple(@RequestParam(name="keyword")String keyword){
		List<DivisionBean> divisionBeans = GBT2260.getAllOnlyDistrictByCity(keyword);
		ResponseEntity<List<DivisionBean>> result = new ResponseEntity<List<DivisionBean>>(divisionBeans, HttpStatus.OK	);
		return result;
	}
}
