package top.hmtools.gbt2260.configuration;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import top.hmtools.GBT2260;

/**
 * 自动配置类，本jar包被其它spring boot 工程引用时的入口启动（初始化）程序
 * @author HyboJ
 * 创建日期：2017-9-26下午7:51:58
 */
@Configuration
@ComponentScan(basePackages={
        "top.hmtools.gbt2260"
        })
public class Gbt2260AutoConfiguration{
	
	/**
	 * 初始化
	 */
	@PostConstruct
	public void init(){
		GBT2260.init();
	}
}
